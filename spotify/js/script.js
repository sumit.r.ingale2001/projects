let audioElement = new Audio("../songs/1.mp3");
let songIndex = 0;
let gif = document.getElementById("gif");
let masterPlay = document.getElementsByClassName("masterPlay")[0];
let songList = Array.from(document.getElementsByClassName("songitem"));
let SongPlay = Array.from(document.getElementsByClassName("allplay"));
let myProgressBar = document.getElementById("progressBar");

let songs = [
    {
        songName: "warrior",
        filepath: "../songs/1.mp3",
        coverpath: "../covers/1.jpg"
    },
    {
        songName: "Cielo Hume-Huma",
        filepath: "../songs/2.mp3",
        coverpath: "../covers/2.jpg"
    },
    {
        songName: "Heros",
        filepath: "../songs/3.mp3",
        coverpath: "../covers/3.jpg"
    },
    {
        songName: "Music track",
        filepath: "../songs/4.mp3",
        coverpath: "../covers/4.jpg"
    },
    {
        songName: "Janji-Heroes Tonight",
        filepath: "../songs/5.mp3",
        coverpath: "../covers/5.jpg"
    },
    {
        songName: "Maan meri jaan",
        filepath: "../songs/6.mp3",
        coverpath: "../covers/6.jpg"
    },
    {
        songName: "Besharam rang",
        filepath: "../songs/7.mp3",
        coverpath: "../covers/7.jpg"
    },
    {
        songName: "Manike",
        filepath: "../songs/8.mp3",
        coverpath: "../covers/8.jpg"
    },
    {
        songName: "Jhome jo pathan",
        filepath: "../songs/9.mp3",
        coverpath: "../covers/9.jpg"
    },
    {
        songName: "Kahani suno",
        filepath: "../songs/10.mp3",
        coverpath: "../covers/10.jpg"
    },
];

songList.forEach((element, i) => {
    element.getElementsByClassName("coverPath")[0].src = songs[i].coverpath;
    element.getElementsByClassName("songname")[0].innerHTML = songs[i].songName;
})


// progressbar percentage
audioElement.addEventListener("timeupdate", () => {
    progress = parseInt((audioElement.currentTime / audioElement.duration) * 100);
    myProgressBar.value = progress
})
// clickable progress bar
myProgressBar.addEventListener("click", () => {
    audioElement.currentTime = parseInt((myProgressBar.value * audioElement.duration) / 100)
})


// next button 
document.getElementById("next").addEventListener("click", () => {
    if (songIndex >= 9) {
        songIndex = 0;
    }
    else {
        songIndex += 1;
        audioElement.src = `../songs/${songIndex + 1}.mp3`;
        audioElement.currentTime = 0;
        audioElement.play();
        masterPlay.classList.remove("fa-play");
        masterPlay.classList.add("fa-pause");
    }
})

document.getElementById("previous").addEventListener("click", () => {
    if (songIndex >= 9) {
        songIndex = 0;
    }
    else {
        songIndex -= 1;
        audioElement.src = `../songs/${songIndex + 1}.mp3`;
        audioElement.currentTime = 0;
        audioElement.play();
        masterPlay.classList.remove("fa-play");
        masterPlay.classList.add("fa-pause");
    }
})

function makeAllPlay() {
    SongPlay.forEach((element) => {
        element.classList.remove("fa-pause")
        element.classList.add("fa-play");
    })
}

SongPlay.forEach((element) => {
    element.addEventListener("click", (e) => {
        songIndex = parseInt(e.target.id)
        makeAllPlay();
        e.target.classList.remove("fa-play");
        e.target.classList.add("fa-pause");
        audioElement.src = `../songs/${songIndex + 1}.mp3`;
        audioElement.currentTime = 0;
        audioElement.play();
        masterPlay.classList.remove("fa-play");
        masterPlay.classList.add("fa-pause");
    })

})







// masterplay button 
masterPlay.addEventListener("click", () => {
    if (audioElement.paused || audioElement.currentTime <= 0) {
        masterPlay.classList.remove("fa-play");
        masterPlay.classList.add("fa-pause");
        audioElement.play();
        gif.style.opacity = "1";
    }
    else {
        masterPlay.classList.remove("fa-pause");
        masterPlay.classList.add("fa-play");
        audioElement.pause();
        gif.style.opacity = "0";
    }
})