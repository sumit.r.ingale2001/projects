document.addEventListener("DOMContentLoaded", () => {
    const scores = document.getElementsByClassName("score")[0];
    const win = document.getElementsByClassName("win")[0];
    const grid = document.getElementById("grid");
    const cardsArray = [
        {
            name: "cheeseburger",
            img: "../images/cheeseburger.png"
        },
        {
            name: "fries",
            img: "../images/fries.png"
        },
        {
            name: "hotdog",
            img: "../images/hotdog.png"
        },
        {
            name: "ice-cream",
            img: "../images/ice-cream.png"
        },
        {
            name: "milkshake",
            img: "../images/milkshake.png"
        },
        {
            name: "pizza",
            img: "../images/pizza.png"
        },
        {
            name: "cheeseburger",
            img: "../images/cheeseburger.png"
        },
        {
            name: "fries",
            img: "../images/fries.png"
        },
        {
            name: "hotdog",
            img: "../images/hotdog.png"
        },
        {
            name: "ice-cream",
            img: "../images/ice-cream.png"
        },
        {
            name: "milkshake",
            img: "../images/milkshake.png"
        },
        {
            name: "pizza",
            img: "../images/pizza.png"
        },
    ];

    cardsArray.sort(() => {
        return 0.5 - Math.random();
    }
    );

    const ting = () => {
        const tingAudio = new Audio("../audio/ting.mp3");
        tingAudio.currentTime = 0;
        tingAudio.play();
    }

    const match = () => {
        const matchAudio = new Audio("../audio/match.mp3");
        matchAudio.currentTime = 0;
        matchAudio.play();
    }

    const misMatch = () => {
        const misMatchAudio = new Audio("../audio/gameover.mp3");
        misMatchAudio.play();
    }

    const gameWin = () => {
        const gameWinAudio = new Audio("../audio/music.mp3");
        gameWinAudio.currentTime = 0;
        gameWinAudio.play();
    }

    let cardsChosen = [];
    let cardsChosenId = [];
    let cardsWon = [];
    function createBoard() {
        for (let i = 0; i < cardsArray.length; i++) {
            const cards = document.createElement("img");
            cards.setAttribute("src", "../images/blank.png")
            cards.setAttribute("data-id", i);
            grid.appendChild(cards);
            cards.addEventListener("click", flipCard);
        }
    }
    createBoard();



    function flipCard() {
        let cardId = this.getAttribute("data-id");
        cardsChosen.push(cardsArray[cardId].name);
        cardsChosenId.push(cardId);
        this.setAttribute("src", cardsArray[cardId].img)
        if (cardsChosen.length === 2) {
            setTimeout(checkMatch, 500);
        }
    }

    function checkMatch() {
        const cardimg = document.querySelectorAll("img");
        let optionOneId = cardsChosenId[0];
        let optionTwoId = cardsChosenId[1];

        if (cardsChosen[0] == cardsChosen[1]) {
            match();
            alert("You have found a match");
            cardimg[optionOneId].setAttribute("src", "../images/white.png");
            cardimg[optionTwoId].setAttribute("src", "../images/white.png");
            cardsWon.push(cardsChosen);

            cardimg[optionOneId].removeEventListener("click", flipCard)
            cardimg[optionTwoId].removeEventListener("click", flipCard)
        }
        else {
            misMatch();
            alert("Please try another match");
            cardimg[optionOneId].setAttribute("src", "../images/blank.png");
            cardimg[optionTwoId].setAttribute("src", "../images/blank.png");
        }

        cardsChosen = [];
        cardsChosenId = [];
        scores.textContent = cardsWon.length;
        if (cardsWon.length === cardsArray.length / 2) {
            win.textContent = "Congratulations You Have Won The Game!!!";
            document.getElementsByClassName("gifcontainer")[0].style.display = "block";
            gameWin();
        }
    }

    const cards = document.querySelectorAll("img")
    cards.forEach(e => {
        e.addEventListener("click", () => {
            ting();
        })
    })

})
